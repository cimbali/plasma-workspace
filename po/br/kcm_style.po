# KDE breton translation
# Thierry Vignaud <tvignaud@mandriva.com>, 2004-2005
msgid ""
msgstr ""
"Project-Id-Version: kcmstyle\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-04 01:01+0000\n"
"PO-Revision-Date: 2004-07-08 17:18+0200\n"
"Last-Translator: Thierry Vignaud <tvignaud.com>\n"
"Language-Team: Brezhoneg <Suav.Icb.fr>\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: gtkpage.cpp:58
#, kde-format
msgid "%1 is not a valid GTK Theme archive."
msgstr ""

#: kcmstyle.cpp:161 kcmstyle.cpp:168
#, kde-format
msgid "There was an error loading the configuration dialog for this style."
msgstr ""

#: kcmstyle.cpp:274
#, kde-format
msgid "Failed to apply selected style '%1'."
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:32
#, fuzzy, kde-format
#| msgid "Sho&w icons on buttons"
msgid "Show icons:"
msgstr "&Diskouez an arlunioù war an nozelioù"

#: package/contents/ui/EffectSettingsPopup.qml:33
#, fuzzy, kde-format
#| msgid "Button"
msgid "On buttons"
msgstr "Nozel"

#: package/contents/ui/EffectSettingsPopup.qml:44
#, kde-format
msgid "In menus"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:56
#, kde-format
msgid "Main toolbar label:"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:58
#, kde-format
msgid "None"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:59
#, fuzzy, kde-format
#| msgid "Text Only"
msgid "Text only"
msgstr "Skrid hepken"

#: package/contents/ui/EffectSettingsPopup.qml:60
#, fuzzy, kde-format
#| msgid "Text Alongside Icons"
msgid "Beside icons"
msgstr "Skrid ouzh an arlun"

#: package/contents/ui/EffectSettingsPopup.qml:61
#, fuzzy, kde-format
#| msgid "Text Under Icons"
msgid "Below icon"
msgstr "Skrid dindan an arlun"

#: package/contents/ui/EffectSettingsPopup.qml:76
#, kde-format
msgid "Secondary toolbar label:"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:18
#, fuzzy, kde-format
#| msgid "Application Level"
msgid "GNOME/GTK Application Style"
msgstr "Live ar meziant"

#: package/contents/ui/GtkStylePage.qml:44
#, kde-format
msgid "GTK theme:"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:69
#, fuzzy, kde-format
#| msgid "Preview"
msgid "Preview…"
msgstr "Rakgwel"

#: package/contents/ui/GtkStylePage.qml:88
#, kde-format
msgid "Install from File…"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:93
#, fuzzy, kde-format
#| msgid "Application Level"
msgid "Get New GNOME/GTK Application Styles…"
msgstr "Live ar meziant"

#: package/contents/ui/GtkStylePage.qml:109
#, kde-format
msgid "Select GTK Theme Archive"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:111
#, kde-format
msgid "GTK Theme Archive (*.tar.xz *.tar.gz *.tar.bz2)"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgid ""
"This module allows you to modify the visual appearance of applications' user "
"interface elements."
msgstr ""

#: package/contents/ui/main.qml:84
#, fuzzy, kde-format
#| msgid "Con&figure..."
msgid "Configure Style…"
msgstr "Ke&fluniañ ..."

#: package/contents/ui/main.qml:104
#, kde-format
msgid "Configure Icons and Toolbars"
msgstr ""

#: package/contents/ui/main.qml:120
#, fuzzy, kde-format
#| msgid "Con&figure..."
msgid "Configure GNOME/GTK Application Style…"
msgstr "Ke&fluniañ ..."

#: styleconfdialog.cpp:21
#, kde-format
msgid "Configure %1"
msgstr "Kefluniañ %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: stylepreview.ui:33
#, kde-format
msgid "Tab 1"
msgstr "Bevennig 1"

#. i18n: ectx: property (text), widget (QPushButton, pushButton)
#: stylepreview.ui:65
#, fuzzy, kde-format
#| msgid "Button"
msgid "Push Button"
msgstr "Nozel"

#. i18n: ectx: property (text), item, widget (QComboBox, comboBox)
#: stylepreview.ui:86
#, kde-format
msgid "Combo box"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBox)
#: stylepreview.ui:96
#, kde-format
msgid "Checkbox"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioButton_2)
#. i18n: ectx: property (text), widget (QRadioButton, radioButton_1)
#: stylepreview.ui:106 stylepreview.ui:116
#, kde-format
msgid "Radio button"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: stylepreview.ui:133
#, kde-format
msgid "Tab 2"
msgstr "Bevennig 2"

#. i18n: ectx: label, entry (widgetStyle), group (KDE)
#: stylesettings.kcfg:9
#, fuzzy, kde-format
#| msgid "Application Level"
msgid "Application style"
msgstr "Live ar meziant"

#. i18n: ectx: label, entry (iconsOnButtons), group (KDE)
#: stylesettings.kcfg:13
#, fuzzy, kde-format
#| msgid "Sho&w icons on buttons"
msgid "Show icons on buttons"
msgstr "&Diskouez an arlunioù war an nozelioù"

#. i18n: ectx: label, entry (iconsInMenus), group (KDE)
#: stylesettings.kcfg:17
#, fuzzy, kde-format
#| msgid "Sho&w icons on buttons"
msgid "Show icons in menus"
msgstr "&Diskouez an arlunioù war an nozelioù"

#. i18n: ectx: label, entry (toolButtonStyle), group (Toolbar style)
#: stylesettings.kcfg:23
#, kde-format
msgid "Main toolbar label"
msgstr ""

#. i18n: ectx: label, entry (toolButtonStyleOtherToolbars), group (Toolbar style)
#: stylesettings.kcfg:27
#, kde-format
msgid "Secondary toolbar label"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Thierry Vignaud, Jañ-Mai Drapier"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "tvignaud@mandriva.com, jdrapier@club-internet.fr"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "Application Style"
#~ msgstr "Live ar meziant"

#, fuzzy
#~| msgid "(c) 2002 Karol Szwed, Daniel Molkentin"
#~ msgid "(c) 2002 Karol Szwed, Daniel Molkentin, (c) 2019 Kai Uwe Broulik "
#~ msgstr "(c) 2002 Karol Szwed, Daniel Molkentin"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "Download New GNOME/GTK2 Application Styles..."
#~ msgstr "Live ar meziant"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "GNOME/GTK2 Application Styles"
#~ msgstr "Live ar meziant"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "Download New GNOME/GTK3 Application Styles..."
#~ msgstr "Live ar meziant"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "GNOME/GTK3 Application Styles"
#~ msgstr "Live ar meziant"

#~ msgid "Unable to Load Dialog"
#~ msgstr "N'hellan ket kargñ ar prenestr"

#~ msgid "No description available."
#~ msgstr "N'eus deskrivadur ebet da gaout."

#, fuzzy
#~| msgid "&Toolbar"
#~ msgid "Toolbars"
#~ msgstr "&Barrenn ostilhoù"

#, fuzzy
#~| msgid "Widget Style"
#~ msgid "Widget Style"
#~ msgstr "Giz ar widget"

#, fuzzy
#~| msgid "Widget Style"
#~ msgid "Widget style:"
#~ msgstr "Giz ar widget"

#~ msgid "Description: %1"
#~ msgstr "Deskrivadur : %1"

#~ msgid "KDE Style Module"
#~ msgstr "Mollad ar c'hiz KDE"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "In application"
#~ msgstr "Live ar meziant"

#, fuzzy
#~| msgid "Application Level"
#~ msgid "Global Menu widget"
#~ msgstr "Live ar meziant"

#~ msgid "kcmstyle"
#~ msgstr "kcmstyle"

#~ msgid "Icons Only"
#~ msgstr "Arlunoù hepken"

#, fuzzy
#~| msgid "Text pos&ition:"
#~ msgctxt "@label:listbox"
#~ msgid "Text pos&ition of toolbar elements:"
#~ msgstr "Lec'h ar skrid :"

#, fuzzy
#~| msgid "&Style"
#~ msgctxt "@title:tab"
#~ msgid "&Style"
#~ msgstr "&Giz"

#, fuzzy
#~| msgid "E&nable tooltips"
#~ msgctxt "@option:check"
#~ msgid "E&nable tooltips"
#~ msgstr "Bevaat al lagadennoù"

#~ msgid "Disable"
#~ msgstr "Marvaat "

#~ msgid "Animate"
#~ msgstr "Enaouiñ"

#~ msgid "Me&nu tear-off handles:"
#~ msgstr "Stang Meadow"

#~ msgid "0%"
#~ msgstr "0%"

#~ msgid "50%"
#~ msgstr "50%"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Button Group"
#~ msgstr "Strollad an nozelioù"
