# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Goodhorse <franklin@goodhorse.idv.tw>, 2008.
# Franklin Weng <franklin@goodhorse.idv.tw>, 2010, 2011, 2013, 2014, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016, 2017.
# pan93412 <pan93412@gmail.com>, 2018, 2019, 2020.
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009, 2010.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-02 00:48+0000\n"
"PO-Revision-Date: 2023-01-15 18:50+0800\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kisaragi Hiu"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mail@kisaragi-hiu.com"

#: imagepackage/contents/ui/config.qml:94
#, kde-format
msgid "Positioning:"
msgstr "位置："

#: imagepackage/contents/ui/config.qml:97
#, kde-format
msgid "Scaled and Cropped"
msgstr "調整大小並切割"

#: imagepackage/contents/ui/config.qml:101
#, kde-format
msgid "Scaled"
msgstr "調整大小"

#: imagepackage/contents/ui/config.qml:105
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "調整大小，並保持比例"

#: imagepackage/contents/ui/config.qml:109
#, kde-format
msgid "Centered"
msgstr "置中"

#: imagepackage/contents/ui/config.qml:113
#, kde-format
msgid "Tiled"
msgstr "鋪排"

#: imagepackage/contents/ui/config.qml:141
#, kde-format
msgid "Background:"
msgstr "背景："

#: imagepackage/contents/ui/config.qml:142
#, kde-format
msgid "Blur"
msgstr "模糊"

#: imagepackage/contents/ui/config.qml:151
#, kde-format
msgid "Solid color"
msgstr "純色"

#: imagepackage/contents/ui/config.qml:161
#, kde-format
msgid "Select Background Color"
msgstr "設定背景顏色"

#: imagepackage/contents/ui/config.qml:207
#, kde-format
msgid "Add Image…"
msgstr "新增影像…"

#: imagepackage/contents/ui/config.qml:213
#: slideshowpackage/contents/ui/SlideshowComponent.qml:257
#, kde-format
msgid "Get New Wallpapers…"
msgstr "取得新桌布…"

#: imagepackage/contents/ui/main.qml:66
#, kde-format
msgid "Open Wallpaper Image"
msgstr "開啟桌布影像"

#: imagepackage/contents/ui/main.qml:67
#, kde-format
msgid "Next Wallpaper Image"
msgstr "下一個桌布影像"

#: imagepackage/contents/ui/WallpaperDelegate.qml:34
#, kde-format
msgid "Open Containing Folder"
msgstr "開啟存放資料夾"

#: imagepackage/contents/ui/WallpaperDelegate.qml:40
#, kde-format
msgid "Restore wallpaper"
msgstr "回復桌布"

#: imagepackage/contents/ui/WallpaperDelegate.qml:45
#, kde-format
msgid "Remove Wallpaper"
msgstr "移除桌布"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr "這個工具讓您將影像設定為 Plasma 工作階段的桌布。"

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"希望設定為您的 Plasma 工作階段的桌布用的，一個影像檔或是已安裝的桌布 kpackage"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"這個桌布的檔案名稱中有一個獨自存在的單引號 (') —— 請聯繫桌布作者修正它，或是"
"自己重新命名檔案：%1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "嘗試設定 Plasma 桌布時出錯：\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr "成功設定所有桌面的桌布為基於 KPackage 的 %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "成功設定所有桌面的桌布為影像 %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr "傳遞來要設成桌布的檔案不存在，或我們無法將其辨識為桌布：%1"

#: plugin/finder/packagefinder.cpp:148
#, kde-format
msgid "Recommended wallpaper file"
msgstr "建議桌布檔案"

#: plugin/imagebackend.cpp:243
#, kde-format
msgid "Directory with the wallpaper to show slides from"
msgstr "桌布輪播之圖片目錄"

#: plugin/imagebackend.cpp:353
#, kde-format
msgid "Open Image"
msgstr "開啟影像"

#: plugin/imagebackend.cpp:353
#, kde-format
msgid "Image Files"
msgstr "影像檔"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:38
#, kde-format
msgid "Order:"
msgstr "順序："

#: slideshowpackage/contents/ui/SlideshowComponent.qml:45
#, kde-format
msgid "Random"
msgstr "隨機"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:49
#, kde-format
msgid "A to Z"
msgstr "A 到 Z"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:53
#, kde-format
msgid "Z to A"
msgstr "Z 到 A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:57
#, kde-format
msgid "Date modified (newest first)"
msgstr "修改日期（由新到舊）"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:61
#, kde-format
msgid "Date modified (oldest first)"
msgstr "修改日期（由舊到新）"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:86
#, kde-format
msgid "Group by folders"
msgstr "依資料夾分組"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:98
#, kde-format
msgid "Change every:"
msgstr "變更圖片頻率："

#: slideshowpackage/contents/ui/SlideshowComponent.qml:108
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1小時"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:128
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1分"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:148
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1秒"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:162
#, kde-format
msgid "Folders"
msgstr "資料夾"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, kde-format
msgid "Remove Folder"
msgstr "移除資料夾"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:225
#, kde-format
msgid "Open Folder"
msgstr "開啟資料夾"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:235
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "尚未設定桌布位置"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:250
#, kde-format
msgid "Add Folder…"
msgstr "新增資料夾…"

#~ msgid "There are no wallpapers in this slideshow"
#~ msgstr "此投影播放中沒有桌布"

#~ msgid "Add Custom Wallpaper"
#~ msgstr "新增自訂桌布"

#~ msgid "Remove wallpaper"
#~ msgstr "移除桌布"

#~ msgid "%1 by %2"
#~ msgstr "%1，由：%2"

#~ msgid "Wallpapers"
#~ msgstr "桌布"

#~ msgctxt "<image> by <author>"
#~ msgid "By %1"
#~ msgstr "作者：%1"

#~ msgid "Download Wallpapers"
#~ msgstr "下載新桌布"

#~ msgid "Hours"
#~ msgstr "小時"
