# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2014, 2015, 2016, 2017, 2018.
# Alexander Yavorsky <kekcuha@gmail.com>, 2021, 2022, 2023.
# Olesya Gerasimenko <translation-team@basealt.ru>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-02 01:06+0000\n"
"PO-Revision-Date: 2023-01-22 16:37+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/ui/AlbumArtStackView.qml:181
#, kde-format
msgid "No title"
msgstr "Без названия"

#: package/contents/ui/AlbumArtStackView.qml:181
#: package/contents/ui/main.qml:82
#, kde-format
msgid "No media playing"
msgstr "Ничего не воспроизводится"

#: package/contents/ui/ExpandedRepresentation.qml:413
#: package/contents/ui/ExpandedRepresentation.qml:534
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: package/contents/ui/ExpandedRepresentation.qml:563
#, kde-format
msgid "Shuffle"
msgstr "Перемешать"

#: package/contents/ui/ExpandedRepresentation.qml:589
#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Предыдущая дорожка"

#: package/contents/ui/ExpandedRepresentation.qml:611
#: package/contents/ui/main.qml:100
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Приостановить"

#: package/contents/ui/ExpandedRepresentation.qml:611
#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Воспроизведение"

#: package/contents/ui/ExpandedRepresentation.qml:629
#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Следующая дорожка"

#: package/contents/ui/ExpandedRepresentation.qml:652
#, kde-format
msgid "Repeat Track"
msgstr "Повторять дорожку"

#: package/contents/ui/ExpandedRepresentation.qml:652
#, kde-format
msgid "Repeat"
msgstr "Автоповтор"

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Показать проигрыватель"

#: package/contents/ui/main.qml:116
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Остановить"

#: package/contents/ui/main.qml:125
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Закрыть проигрыватель"

#: package/contents/ui/main.qml:238
#, kde-format
msgid "Choose player automatically"
msgstr "Выбирать проигрыватель автоматически"

#: package/contents/ui/main.qml:292
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"%1 (%2)\n"
"Приостановить: нажатие средней кнопки мыши\n"
"Изменить громкость: прокручивание колёсика мыши"

#: package/contents/ui/main.qml:292
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"%1\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"%1\n"
"Приостановить: нажатие средней кнопки мыши\n"
"Изменить громкость: прокручивание колёсика мыши"

#: package/contents/ui/main.qml:303
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"%1 (приостановлено, %2)\n"
"Продолжить: нажатие средней кнопки мыши\n"
"Изменить громкость: прокручивание колёсика мыши"

#: package/contents/ui/main.qml:303
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"Paused (%1)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"Воспроизведение приостановлено (%1)\n"
"Продолжить: нажатие средней кнопки мыши\n"
"Изменить громкость: прокручивание колёсика мыши"

#~ msgid "General"
#~ msgstr "Основное"

#~ msgid "Volume step:"
#~ msgstr "Шаг изменения громкости:"

#~ msgctxt "by Artist (player name)"
#~ msgid "by %1 (%2)"
#~ msgstr "от %1 (%2)"

#~ msgctxt "Paused (player name)"
#~ msgid "Paused (%1)"
#~ msgstr "Воспроизведение приостановлено (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "от %1"

#~ msgid "Pause playback when screen is locked"
#~ msgstr "Приостанавливать воспроизведение при блокировке экрана"
